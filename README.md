# eXtremeComponents

#### 介绍
1.	该组件在传统ssh架构项目中使用较为普遍，提供便捷的分页、导出等功能，但是官方已经停更很久！
2.	新建 master 分支，修复若干缺陷，优化若干功能，持续更新中……
3.	新建 1.0.1 分支，基于eXtremeComponents1.0.1改造成maven工程，为传统项目的持续维护提供支持。



#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx


#### 使用说明

1.  [eXtremeComponents参考文档](http://www.blogjava.net/lucky/articles/33380.html)
2.  [支持PDF中文导出](http://http://www.blogjava.net/lucky/archive/2006/03/archive/2006/03/10/34717.html)
3.  主干版本使用补充说明

3.1. PDF导出时支持各种字体，如：

3.1.1. 使用org.apache.fop.fonts.apps.TTFReader生成中文宋体对应的度量文件SimSun.xml
```
String[] args = { "-ttcname", "SimSun",
		"c:\\windows\\fonts\\simsun.ttc", 
		"SimSun.xml" };
TTFReader.main(args);
```
FOP默认依赖包xml-apis,xercesImpl,xalan；

JDK1.5+已自带相关API，与依赖包冲突，影响TTFReader，故现默认剔除依赖包；

若仍是JDK1.4，可自行引入依赖包；

3.1.2.  定义FOP对字体的配置文件userconfig.xml，参考FOP包内置的/conf/userconfig.xml
这里baseDir可以定义度量文件(metrics-file)的根路径，fontBaseDir可以定义字体文件（embed-file）的根路径，部署环境切换时，只需更改根路径即可

```
  <entry>
    <key>baseDir</key>
    <value>D:\Users\charlie\git\ss2h\ss2h-01\</value>
  </entry>

  <entry>
    <key>fontBaseDir</key>
    <value>C:\windows\fonts\</value>
  </entry>

 <!-- zh_CN fonts -->
 <font metrics-file="SimSun.xml" embed-file="simsun.ttc" kerning="yes">
    <font-triplet name="SimSun" style="normal" weight="normal"/>
    <font-triplet name="SimSun" style="normal" weight="bold"/>
    <font-triplet name="SimSun" style="italic" weight="normal"/>
    <font-triplet name="SimSun" style="italic" weight="bold"/>
 </font>
```

3.1.3.  ec配置文件中定义

```
exportPdf.userconfigLocation=/userconfig.xml
exportPdf.font=SimSun
```

3.1.4.  部署环境OS不同时，重新执行3.1.1.

3.2.  其他优化

3.2.1.  PDF导出时，表头支持国际化；

3.2.2.  精简依赖，不再内置SiteMesh，故剔除SiteMesh包，剔除org.extremecomponents.table.filter.SitemeshPageFilter；

3.2.3.  鉴于多处复用，故抽取国际化工具类org.extremecomponents.util.I18nUtils

3.2.4. 增加国际化支持:calcTitle，fileName；增加导出PDF头默认色：背景gray,内容black；国际化key不再要求含英文句点；

4. 详细使用可以参考[ss2h](http://gitee.com/tingfangcao/ss2h.git)/ss2h-01  

#### 参与贡献

1.  Fork 本仓库
2.  提交代码
3.  新建 Pull Request

#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
